import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent {

  sArtists:any[] = []
  loading:boolean;

  error:boolean = false;
  errorMessage:string = '';

  constructor(private spotifyService:SpotifyService) { }

  buscar(term){
    if(term){
      this.loading = true;
    }
    this.spotifyService.getArtists(term).subscribe( response => {
      this.sArtists = response
      console.log(this.sArtists);
      this.loading = false;
    }, (serviceError) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = serviceError.error.error.message
    })
  }

}
