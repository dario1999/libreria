import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})

export class HomeComponent {

  newSongs:any[] = [];
  loading:boolean;

  error:boolean = false;
  errorMessage:string = '';

  constructor( private spotifyService:SpotifyService ) {
    this.loading = true
    this.spotifyService.getNewReleases().subscribe( response => {
      this.newSongs = response
      console.log(this.newSongs);
      this.loading = false;
    }, (serviceError) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = serviceError.error.error.message
    })
  }


}
