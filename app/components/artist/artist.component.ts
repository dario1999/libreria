import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SpotifyService } from '../../services/spotify.service';
@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: [
  ]
})
export class ArtistComponent{

  artist:any = {};
  topTracks:any[] = [];
  loading:boolean

  constructor( private route:ActivatedRoute,
               private spotifyService:SpotifyService ) {

    this.route.params.subscribe( params => {

      console.log(params['id']);
      this.getArtista( params['id'] );
      this.getTopTracks( params['id'] );
    })

   }

  getArtista( id:string ){
    this.loading = true;
    this.spotifyService.getArtist( id ).subscribe( response => {
      console.log(response);
      this.artist = response
      this.loading = false;
    })
  }

  getTopTracks( id:string ){
    this.spotifyService.getTopTracks( id ).subscribe( response => {
      console.log(response);
      this.topTracks = response
    })
  }
}
