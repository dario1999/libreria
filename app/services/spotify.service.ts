import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http:HttpClient ) {
    console.log('spotify arrancado, configurado y listo para ser ejecutado')
  }

  getQuery( query:string ){
    const url = `https://api.spotify.com/v1/${ query }`

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBBj3AHn5aOUThQfV0Qu8Fu-smtfMfJsieXQXReCJP4dXYIHGjuKyhN_mIUWrsaxYxiOl8zW-wn62GsDVo'
    });

    return this.http.get(url, { headers } )
  }

  getNewReleases(){
    return this.getQuery('browse/new-releases').pipe( map( response => response['albums'].items));
  }

  getArtists(term){
    return this.getQuery(`search?q=${ term }&type=artist&limit=10`).pipe( map( response => response['artists'].items));
  }

  getArtist( id:string ){
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracks( id:string ){
    return this.getQuery(`artists/${ id }/top-tracks?market=US`).pipe( map( response => response['tracks']));
  }
}
